# -*- coding: utf-8 -*-

__author__ = 'egr'

ID = "id"
CLASS = "class"
TITLE = "title"
CHILDS = "childs"

MAP = [
    {
        ID: "portfolio.index",
        CLASS: "icon-briefcase",
        TITLE: u"Портфель"
    },
    {
        ID: "news.index",
        CLASS: "icon-book",
        TITLE: u"Блог"
    },
    {
        ID: "search",
        CLASS: "icon-search",
        TITLE: u"Поиск"
    },
    {
        ID: "news.recent_feed",
        CLASS: "icon-rss",
        TITLE: u"RSS"
    },
]